class Victim:
  def __init__(self, hp, velocity, mult, distance):
    self.hp = int(hp)
    self.velocity = int(velocity)
    self.mult = int(mult)
    self.distance = int(distance)

  def __repr__(self):
    return "victim"