class Field:
  def __init__(self, height, width):
    self.height = height
    self.width = width
    self.field = [[0 for i in range(height)] for j in range(width)]

  def __repr__(self):
    for i in range(self.height):
      for j in range(self.width):
        print(self.field[i][j], end=" ")
      print("\n")
    return " "

  def insert(self, obj, pos):
    self.field[pos[0]][pos[1]] = obj

  def erase(self, pos):
    self.field[pos[0]][pos[1]] = 0
  
  def clear(self):
    self.field = [[0 for i in range(self.height)] for j in range(self.width)]