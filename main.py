from Field import Field
from Predator import Predator
from Victim import Victim
from Corn import Corn

h, w = list(map(int, input("Enter dimensions of field: ").split()))
f = Field(h, w)
while True:
  print(f)
  print("Enter objects? (Yes/No)")
  obj = input()
  if obj == "No":
    break
  else:
    obj = obj.split()
    if obj[0] == "Predator":
      f.insert(Predator(obj[3], obj[4], obj[5]), [int(obj[1])][int(obj[2])])
    elif obj[0] == "Victim":
      f.insert(Victim(obj[3], obj[4], obj[5], obj[6]), [int(obj[1])][int(obj[2])])
    elif obj[0] == "Corn":
      f.insert(Corn(obj[3]), [int(obj[1])][int(obj[2])])
